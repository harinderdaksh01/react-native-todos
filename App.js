import React from 'react';
import type {Node} from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Task from "./components/Task";
import BgChanges from "./components/BGChanges";
const App = () => {
    return (
        // <View style={styles.container}>
        //     <View style={styles.tasksWrapper}>
        //         <Text style={styles.sectionTitle}>Today's tasks</Text>
        //
        //         <View style={styles.items}>
        //
        //             <Task text={"task 1"} />
        //             <Task text={"task 2"} />
        //             {/*<Task/>*/}
        //             {/*<Task/>*/}
        //             {/*<Task/>*/}
        //             {/*<Task/>*/}
        //         </View>
        //     </View>
        // </View>
      <BgChanges />
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#E8EAED"
    },
    tasksWrapper: {
        paddingTop: 80,
        paddingHorizontal: 20,

    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    items: {},


});


export default App;
