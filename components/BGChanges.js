import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text, TouchableOpacity,
  useColorScheme,
  View,

} from "react-native";



const BgChanges = () => {
  const [randomColor,setRandomColor] = useState("rgb(32,0,126)")

  const changeBg = () =>{
   let color='rgb(' +
     Math.floor(Math.random() * 256)+
     ',' +
     Math.floor(Math.random() * 256)+
     ',' +
     Math.floor(Math.random() * 256)+
     ')';
     setRandomColor(color)
    console.log(color)

  }
  return (
    <>
      <StatusBar backgroundColor={randomColor} />
    <View style={[styles.containers,{backgroundColor:randomColor}]}>
      <TouchableOpacity onPress={changeBg}>
      <Text style={styles.text}>Click Here</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btn} onPress={()=>setRandomColor("rgb(1,1,1)")}>
      <Text style={styles.text}>Reset Btn</Text>
      </TouchableOpacity>
      </View>
    </>
  );
};
const styles=StyleSheet.create({
  containers:{
    flex:1,
    alignItems:'center',
    justifyContent:"center",
    backgroundColor:"#6A5ACC"
  },
  text:{
    backgroundColor: "#fda500",
    padding:10,
    fontsize:10,
    borderRadius:10,
    cursor:'pointer'
  },
  btn:{
    marginTop:2,
    color:'#fff'
  }
})
export default BgChanges;
